FROM haproxy:1.8.14-alpine
COPY haproxy.cfg /usr/local/etc/haproxy/haproxy.cfg

LABEL version="0.5-beta"
LABEL type="haproxy"

EXPOSE 80/tcp
EXPOSE 80/udp
EXPOSE 443/tcp
EXPOSE 443/udp
EXPOSE 8080/tcp